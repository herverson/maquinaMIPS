PROJETO EM MIPS Máquina de cartão de crédito
============================================

1. O programa deve solicitar ao usuário e receber o tipo de pagamento escolhido pelo
cliente (crédito ou débito).
2. O programa deve solicitar ao usuário e receber um código referente a bandeira do
cartão (1-Visa/2-Master/3-Elo).
3. O programa deve solicitar ao usuário o valor a ser pago.
4. O programa deve solicitar a leitura do cartão.
a. O número do cartão deve ser digitado simulando a leitura.
5. O programa deve solicitar a senha do cliente.
6. O programa deve exibir na tela as seguintes informações:<br />
    a. O tipo de pagamento selecionado;<br />
    b. O valor da compra;<br />
    c. O número do cartão; <br />

## Autor
Herverson
