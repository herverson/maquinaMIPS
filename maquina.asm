.data
	# mensagens para pedir as informações
	creditodebito: .asciiz "\n\n1 - crédito ou 2 - débito 3 - desligar\n"
	bandeira: .asciiz "1-Visa/2-Master/3-Elo\n"
	valorapagar: .asciiz "Informe o valor a ser pago\n "
	numerocartao: .asciiz "Informe o número do cartão\n "
	senha: .asciiz "\nInforme a senha\n "
	# mensagens de resultado
	tipopagamentoDebito: .asciiz "\n\nTipo de pagamento: Débito"
	tipopagamentoCredito: .asciiz "\n\nTipo de pagamento: Crédito"
	valorDacompra: .asciiz "\nValor da compra : R$"
	numerocartaoDisplay: .asciiz "\nO número do cartão : "
	tamanhocartao: .space 17
	
.text
main:
	
	loop:
	li $v0, 4 # comando de impressão na tela
	la $a0, creditodebito # pergunta se é crédito ou débito
	syscall # efetua a chamada ao sistema
	
	li $v0, 5 # le entrada do usuário
	syscall # faz chamada ao sistema
	
	move $s0, $v0 # move conteúdo de $v0 para $s0
	beq $s0,1,credito # verifica se $s0 é igual a 1 se for desvia para credito
	beq $s0,2,debito # verifica se $s0 é igual a 2 se for desvia para debito
	beq $s0,3,exit
	j loop # desvia para termina
credito:
	li $v0, 4 # comando de impressão na tela
	la $a0, bandeira  # pede a bandeira do cartão
	syscall # efetua a chamada ao sistema
	
	li $v0, 5 # le entrada do usuário
	syscall # efetua a chamada ao sistema
	
	move $s1, $v0 # move conteúdo de $v0 para $s1
	
	li $v0,4 # comando de impressão na tela
	la $a0, valorapagar # pede o valor a ser pago
	syscall # efetua a chamada ao sistema
	
	li $v0, 7 #le entrada do usuário double
	syscall # efetua a chamada ao sistema
	
	move $s2, $v0 # move conteúdo de $v0 para $s2
	
	li $v0,4 # comando de impressão na tela
	la $a0, numerocartao # pede o número do cartão
	syscall # efetua a chamada ao sistema
	
	li $v0, 8       # le entrada usuario do tipo string

    la $a0, tamanhocartao  # carrega byte space no endereço
    li $a1, 17      # aloca o byte space para a string

    move $s3, $a0   # salva string para s3
    syscall
	
	li $v0,4 # comando de impressão na tela
	la $a0, senha # pede a senha do cartão
	syscall # efetua a chamada ao sistema
	
	li $v0, 5 #le entrada do usuário
	syscall # efetua a chamada ao sistema
	
	move $s4, $v0 # move conteúdo de $v0 para $s4
	li $v0, 4 # comando de impressão na tela
	la $a0, tipopagamentoCredito  #coloca o texto do tipo de pagamento credito para ser impresso
	syscall # efetua a chamada ao sistema
	
	j imprimir #desvia para termina
		
debito:
	li $v0, 4 # comando de impressão na tela
	la $a0, bandeira  # pede a bandeira do cartão
	syscall # efetua a chamada ao sistema
	
	li $v0, 5 # le entrada do usuário
	syscall # efetua a chamada ao sistema
	
	move $s1, $v0 # move conteúdo de $v0 para $s1
	
	li $v0,4 # comando de impressão na tela
	la $a0, valorapagar # pede o valor a ser pago
	syscall # efetua a chamada ao sistema
	
	li $v0, 7 #le entrada do usuário double
	syscall # efetua a chamada ao sistema
	
	move $s2, $v0 # move conteúdo de $v0 para $s2
	
	li $v0,4 # comando de impressão na tela
	la $a0, numerocartao # pede o número do cartão
	syscall # efetua a chamada ao sistema
	
	li $v0, 8       # le entrada usuario do tipo string

    la $a0, tamanhocartao  # carrega byte space no endereço
    li $a1, 17      # aloca o byte space para a string

    move $s3, $a0   # salva string para s3
    syscall
	
	li $v0,4 # comando de impressão na tela
	la $a0, senha # pede a senha do cartão
	syscall # efetua a chamada ao sistema
	
	li $v0, 5 #le entrada do usuário
	syscall # efetua a chamada ao sistema
	
	move $s4, $v0 # move conteúdo de $v0 para $s4
	li $v0, 4 # comando de impressão na tela
	la $a0, tipopagamentoDebito  #coloca o texto do tipo de pagamento debito para ser impresso
	syscall # efetua a chamada ao sistema
	
	j imprimir #desvia para termina
	
imprimir:

	# imprime o valor da compra
	li $v0, 4 # comando de impressão na tela
	la $a0, valorDacompra  #coloca o texto do tipo de pagamento para ser impresso
	syscall # efetua a chamada ao sistem
	
	li $v0, 3
	add.d $f12, $f0, $f10
	syscall
	
	
	# imprime o número do cartão
	li $v0, 4 # comando de impressão na tela
	la $a0, numerocartaoDisplay  #coloca o texto do número do cartão para ser impresso
	syscall # efetua a chamada ao sistem
	
	la $a0, tamanhocartao  # regarrega o byte space para o endereço primário
    move $a0, $s3   # endereço primário = endereço s3  (carrega o  ponteiro)
    li $v0, 4       # print string
    syscall
    j loop
	
	
exit:	

	# fim do programa	
	li $v0, 10 # comando de exit
	syscall # efetua a chamada ao sistema	
